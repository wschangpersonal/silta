<?php

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index() {
    date_default_timezone_set('Asia/Taipei');
//    require_once "phpMQTT.php";
//    $mqtt = new phpMQTT('3egreenserver.ddns.net', 1999, "Client ID".rand());
//    if ($mqtt->connect(true, NULL)) {
//      echo "connect success";
//    }
//    $data = json_decode(file_get_contents("http://ble.aengin.com:8080/headcount/?agentIds=2471895BF6EA+2471895BF694+2471895BF6E6+2471895BF9A4+2471895BF6EE+2471895BF6AE+2471895BF9BC+2471895BF6AA+2471895BF796+2471895BF9AC&countDown=1"));
//    $data = json_decode(file_get_contents("./application/temp.json"));
    $this->load->view('dashboard', [
        'data' => $this->update(false),
        'agentId' => '2471895BF6EA'
    ]);
  }

  /**
   * DB Relayer - PHP Version
   * @param type $response
   * @return type
   */
  public function update($response = true, $time = null, $prev = -1) {
//    $this->load->library('mongo_db');
    date_default_timezone_set('Asia/Taipei');
    $time1 = microtime(true)*1000;
    require_once 'vendor/autoload.php';
    $db = new MongoDB\Client("mongodb://ble.aengin.com:27017");
    $time2 = microtime(true)*1000;
    $collection = $db->courier->hub_info;
    $hubs = $collection->find([]);
    $result = [];
    foreach ($hubs as $hub) {
      $beacons = [];
      foreach ($hub['conn_beacon'] as $k2 => $beacon) {
        $uuid = bin2hex($beacon['uuid']->getData());

        $data = [
            (int) $beacon['rssi'],
            //beacon mac
            $beacon['beacon'],
            //uuid
            implode('-', [substr($uuid, 0, 8), substr($uuid, 8, 4), substr($uuid, 12, 4), substr($uuid, 16, 4), substr($uuid, 20)]),
            //time
            '----/--/-- --:--:--'
        ];
        if (isset($beacon['time'])) {
          switch (true) {
            case $beacon['time'] instanceof MongoDB\BSON\Timestamp:
//              $data[2] = date('Y-m-d H:i:s', json_decode((string)$beacon['time'])[1]);
              $data[3] = date('Y-m-d H:i:s', explode(':', trim((string) $beacon['time'], '[]'))[1]);
              break;
            case $beacon['time'] instanceof MongoDB\BSON\UTCDateTime:
              $data[3] = $beacon['time']->toDateTime()->format('Y-m-d H:i:s');
              break;
          }
        }
        $beacons[] = $data;
      }
      $result[] = [
          $hub['agentId'], //agentId
          $beacons         //beacons
      ];
    }
    $time3 = microtime(true)*1000;
    if ($response) {
      if ($time) { //寫log
        $fp = fopen(dirname(dirname(__DIR__)).'/delay-log-php.txt', 'a+');
        //Profiling Structure: 
        //  DB Setup Connection Time
        //  Relay2DB + DB2Relay (Fetching collection) Time
        //  Relay2Browser Time
        //  Browser2Relay Time
        $time = $time;
        fwrite($fp, implode(',', [$time2-$time1, $time3 - $time2, $prev, $time1 - $time])."\n");
      }
      header('content-type: application/json; charset=utf-8');
      echo json_encode(['hubs' => $result, 'time' => floor(microtime(true)*1000)]);
    } else {
      return $result;
    }
  }

}
