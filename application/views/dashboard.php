<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="asset/db_css.css">
  </head>
  <body>
    <div class="container-fluid text-center navbar">

      <div class="row content">
        <div class="col-sm-2 sidenav">
          <div id="tops">
            <h1 class="text-center">Silta</h1>
            <h3>Dashboard</h3>
          </div>
          <div><img src="asset/01.png" alt="" class="img-circle img-responsive"></div>
          <h4 class="menu2">Admin</h4>
          <h4 class="menu"><a href="#">Overview</a></h4>
          <h4 class="menu"><a href="#">Beacon</a></h4>
          <h4 class="menu"><a href="#">Report</a></h4>
          <div id="foot">	
            <hr>
            <h2 id="info">Silta</h2>
            <h4 id="contact">Designed for Security</h3>
              <p>service@aengin.com<br><a href="http://www.aengin.com/">Powered by Aengin Technology Inc.</a></p>
          </div>

        </div>
        <div class="clear"></div>

        <div class="col-sm-10-offset-2 text-left db">
          <h4 class="mtitle">Map</h4>
          <div class="map"><img src="asset/map-empty.png" alt="" class="img-responsive"></div>
          <table class="table table-hover col-md-9 table-responsive">
            <h4 class="mtitle">Beacon Information</h4>
            <thead>
              <tr>
                <th>Device</th>
                <th>Major</th>
                <th>Minor</th>
                <th>Battery</th>
                <th>RSSI</th>
                <th>DSTC(m)</th>
                <th>Last Known</th>
                <th>Button State</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($data as $agent):
//                if (!in_array($agent->agentId, ['2471895BF9AC', '2471895BF9B4']))
//                  continue;
                foreach ($agent[1] as $i => $beacon):
                  ?>
                  <tr>
                    <td><p class="detitle">Beacon <?= $i + 1 ?></p><br><p class="gray">MACAddress <?= $beacon[1] ?><br>UUID  <?= $beacon[2] ?></p></td>
                    <td class="mj">12</td>
                    <td class="mj">469</td>
                    <td class="mj">46%</td>
                    <td class="twoline"><span id="rssi-<?= $beacon[1] ?>-<?= $beacon[2] ?>"><?= $beacon[0] ?></span><br><p class="gray">(33,67,12)</p></td>
                    <td class="mj">12</td>
                    <td class="twoline">Room 325<br><p class="gray"><span class="glyphicon glyphicon-time"></span><span id="time-<?= $beacon[1] ?>-<?= $beacon[2] ?>"><?= $beacon[3] ?></span></p></td>
                    <td class="ok"><span class="glyphicon glyphicon-ok"></span><br>Release</td>
                  </tr>
                  <?php
                endforeach;
              endforeach;
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
      $(document).ready(function () {
        /**
         * php Updater
         */
//        console.log("PHP Relayer start.");
//        var prev = -1;
//        function updater() {
//          setTimeout(function () {
//            var time = (new Date()).getTime();
//            $.get('http://localhost/silta_demo/welcome/update/1/'+time+'/'+prev, function (json) {
//              var count = 0;
//              prev = (new Date()).getTime() - (+json.time);
//              console.log(prev, json.time);
//              json.hubs.forEach((hub) => {
//                hub[1].forEach((beacon) => {
//                  let id = 'rssi-' + beacon[1] + '-' + beacon[2],
//                          node = document.getElementById(id);
//                  if (node) {
//                    count++;
//                    node.innerHTML = beacon[0];
//                    document.getElementById('time-' + beacon[1] + '-' + beacon[2]).innerHTML = beacon[3];
//                  }
//                });
//              });
//              console.log(count+ ' device are updated.');
//              updater();
//            });
//          }, 1);
//        }
//        updater();

        /**
         * WebSocket Updater
         */
        console.log("NodeJS Relayer start.");
        class Relayer {
          constructor(dns = 'ws://localhost:8888') {
            this.dns = dns;
            this.last_update_time = (new Date()).toISOString();
            this.prevNow = performance.now();
            this.timelog = [];
            this.connect();
          }
          connect() {
            console.log('Try connect...');
            let self = this;
            try {
              this.ws = new WebSocket(this.dns);
              this.ws.onopen = () => {
                console.log('Socket open! Send first message to start updating process');
                this.ws.send((new Date()).getTime() + " " + this.last_update_time);
              };
              this.ws.onmessage = (event) => {
                var now = performance.now();
                this.timelog.push(now - this.prevNow);
                this.prevNow = now;
                var count = 0;
                var json = JSON.parse(event.data);
                json.forEach((hub) => {
                  this.last_update_time = hub[1] > this.last_update_time ? hub[1] : this.last_update_time;
                  hub[2].forEach((beacon) => {
                    let id = 'rssi-' + beacon[1] + '-' + beacon[2],
                            node = document.getElementById(id);
                    if (node) {
                      count++;
                      node.innerHTML = beacon[0];
                      document.getElementById('time-' + beacon[1] + '-' + beacon[2]).innerHTML = beacon[3];
                    }
                  });
                });
                console.log(count + ' devices updated. FPS: '+(1000/(this.timelog.reduce((prev, curr) => prev+curr, 0)/this.timelog.length)));
                this.ws.send((new Date()).getTime() + " " + this.last_update_time);
              };
              this.ws.onclose = () => {
                setTimeout(() => this.connect(), 1000);
              };
              return;
            } catch (ex) {
              console.log('Reconnect fail. Retry after 1 second...');
            }
            console.log(this, this.connect);
            setTimeout(() => this.connect(), 1000);
          }
        }
        
        new Relayer();

      });
//      
//      
//      var conn = new WebSocket('ws://localhost:8888');
//      conn.onopen = function (e) {
//        console.log("Connection established!");
//      };
//
//      conn.onmessage = function (e) {
//        var tmp = e.data.split("\n"), mac = tmp[2].split("_")[1];
//        tmp[1] = JSON.parse(tmp[1]);
//        tmp[1].forEach(function(device) {
////          console.log('topic:' + tmp[2],  'rssi-'+mac+'-'+tmp[0]);
//          var element = document.getElementById('rssi-'+mac+'-'+tmp[0]);
//          if (element) {
//            console.log('update!');
//            element.innerHTML = device.RSSI;
//          }
//        });
//        console.log(tmp[0], tmp[1]);
//        conn.close();
//      };
    </script>
  </body>
</html>