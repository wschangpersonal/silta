<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Welcome to CodeIgniter</title>

    <style type="text/css">

      ::selection { background-color: #E13300; color: white; }
      ::-moz-selection { background-color: #E13300; color: white; }

      body {
        background-color: #fff;
        margin: 40px;
        font: 13px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
      }

      a {
        color: #003399;
        background-color: transparent;
        font-weight: normal;
      }

      h1 {
        color: #444;
        background-color: transparent;
        border-bottom: 1px solid #D0D0D0;
        font-size: 19px;
        font-weight: normal;
        margin: 0 0 14px 0;
        padding: 14px 15px 10px 15px;
      }

      code {
        font-family: Consolas, Monaco, Courier New, Courier, monospace;
        font-size: 12px;
        background-color: #f9f9f9;
        border: 1px solid #D0D0D0;
        color: #002166;
        display: block;
        margin: 14px 0 14px 0;
        padding: 12px 10px 12px 10px;
      }

      #body {
        margin: 0 15px 0 15px;
      }

      p.footer {
        text-align: right;
        font-size: 11px;
        border-top: 1px solid #D0D0D0;
        line-height: 32px;
        padding: 0 10px 0 10px;
        margin: 20px 0 0 0;
      }

      #container {
        margin: 10px;
        border: 1px solid #D0D0D0;
        box-shadow: 0 0 8px #D0D0D0;
      }
    </style>
  </head>
  <body>

    <div id="container">
      <h1>Welcome to CodeIgniter!</h1>

      <div id="body">
        <p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

        <p>If you would like to edit this page you'll find it located at:</p>
        <code>application/views/welcome_message.php</code>

        <p>The corresponding controller for this page is found at:</p>
        <code>application/controllers/Welcome.php</code>

        <p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
      </div>
      <div id="dock"></div>
      <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.0.0/pixi.min.js"></script>
    <script>
      
      var render = new PIXI.WebGLRenderer(400, 300, {
        backgroundColor: '#FFFF99'
      });
      var time = (new Date()).getTime(), r = 150, arc, arc2, elapse, x, y;
      document.body.appendChild(render.view);
      var stage = new PIXI.Container();
      stage.interctive = true;
      var graphic = new PIXI.Graphics();
      stage.addChild(graphic);
      var textureButton = PIXI.Texture.fromImage('button_sync.png');
      var textureButtonOk = PIXI.Texture.fromImage('button_ok.png');
      var button = new PIXI.Sprite(textureButton);
      button.buttonMode = true;
      button.anchor.set(0.5);
      button.position.x = 200;
      button.position.y = 150;
      button.interactive = true;
      button.on('mouseover', function () {
        if (this.isOver != true) {
          this.alpha = 0.5;
        }
        this.isOver = true;
      })
      button.on('mouseout', function () {
        if (this.isOver != false) {
          this.alpha = 1;
        }
        this.isOver = false;
      });
      button.on('mouseup', function () {
        this.texture = textureButtonOk;
      });
      stage.addChild(button);
      var boy = new PIXI.Sprite(PIXI.Texture.fromImage('boy.png'));
      boy.anchor.set(0.5);
      boy.log_pos = [
        {x: 20, y: 20},
        {x: 50, y: 33},
        {x: 380, y: 37},
        {x: 389, y: 280},
        {x: 103, y: 202}
      ];
      boy.position.x = boy.log_pos[0].x;
      boy.position.y = boy.log_pos[1].y;
      boy.log_pos_bias = [];
      boy.log_pos_index = 0;
      boy.log_pos_speed = 2; //2秒間隔
      boy.interactive = true;
      boy.on('mouseover', function () {
        if (this.isOver !== true) {
          this.scale.x = 1.2;
          this.scale.y = 1.2;
        }
        this.isOver = true;
      });
      boy.on('mouseout', function () {
        if (this.isOver !== false) {
          this.scale.x = 1;
          this.scale.y = 1;
        }
        this.isOver = false;
      });
      stage.addChild(boy);
      var chat = new PIXI.Sprite(PIXI.Texture.fromImage('chat.png'));
      chat.position.set(boy.position.x + 24, boy.position.y - 12);
      chat.buttonMode = true;
      chat.interactive = true;
      chat.on('mouseup', function() {
        this.alpha = this.alpha == 0.5 ?1:0.5;
      });
      stage.addChild(chat);
      animate();
      function animate() {
        elapsed = (new Date()).getTime() - time;
//          //2秒一圈
        arc = 2 * Math.PI * ((elapsed % 1000) / 1000);
        arc2 = 2 * Math.PI * ((elapsed % 300) / 300);
        r = button.isOver ? 35 + 15 * Math.cos(arc2) : 50;
        x = Math.max(0, Math.min(400, 200 + (2*r * Math.cos(arc))));
        y = Math.max(0, Math.min(300, 150 + (r * Math.sin(arc))));
        graphic
                .clear()
                .beginFill(0xFF0000, 1)
                .lineStyle(2, 0xFFFF00, 1)
                .drawRect(x, y, 10 + 2 * Math.cos(arc2), 10 + 2 * Math.cos(arc2))
                .endFill();
        if (button.isOver) {
          button.rotation += 0.1;
        } else {
          button.rotation = 0;
        }
        //繪製BOY的移動
        if (boy.log_pos_index == 0 || boy.log_pos_count == 0) {
          if (boy.log_pos_index < boy.log_pos.length - 1) {
            boy.log_pos_time = (new Date()).getTime();
            boy.log_pos_count = Math.ceil(boy.log_pos_speed * 1000 / 30);
            boy.log_pos_bias = [(boy.log_pos[boy.log_pos_index + 1].x - boy.log_pos[boy.log_pos_index].x) / boy.log_pos_count, (boy.log_pos[boy.log_pos_index + 1].y - boy.log_pos[boy.log_pos_index].y) / boy.log_pos_count];

          }
          boy.log_pos_index++;
        }
        if (boy.log_pos_index < boy.log_pos.length) {
          boy.position.x = boy.position.x + boy.log_pos_bias[0];
          boy.position.y = boy.position.y + boy.log_pos_bias[1];
          chat.position.set(boy.position.x + 24, boy.position.y - 12);
          boy.log_pos_count--;
        }
        render.render(stage);
        requestAnimationFrame(animate);
      }
//      window.animationFrame = (function (callback) {
//        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
//                function (callback) {
//                  window.setTimeout(callback, 1000 / 5 );
//                };
//      })();
//      
//      function Animation() {
//        var self = this;
//        var time = (new Date()).getTime();
//        var ctx = document.getElementById('cv').getContext('2d');
//        self.drawDot = function(point) {
//          ctx.fillStyle = "#FF0000";
//          ctx.fillRect(point.x, point.y, 5, 5);
//          ctx.strokeStyle = "#FF0000";
//          ctx.stroke();
//        };
//        self.animate = function() {
//          var elapsed = (new Date()).getTime() - time;
//          //2秒一圈
//          var arc = 2*Math.PI * ((elapsed%2000)/2000);
//          var r = 150, point = {
//            x: Math.max(0, Math.min(400, 200 + ( r*Math.cos(arc) ))) ,
//            y: Math.max(0, Math.min(300, 150 + ( r*Math.sin(arc) )))
//          };
//          
//          ctx.clearRect(0, 0, 400, 300);
//          this.drawDot(point);
//          animationFrame(function() { self.animate(); });
//        };
//      }
//      
//      (new Animation()).animate();
    </script>
  </body>
</html>