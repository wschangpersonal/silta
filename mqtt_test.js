/*
 * 對以web socket連接上的對象傳送MQTT原始資料
 * cmd: node mqtt_test
 */
var mqtt = require('mqtt')
var fs = require('fs');
var logname = './delay-log-mqtt.csv';
var log = fs.appendFileSync(logname, '', {flag: 'a+'});
var UUID = require('pure-uuid');
var ws = require('nodejs-websocket').createServer(function (conn) {
  console.log('new ws connected.');
  conn.on('close', function () {
    console.log('ws closed.');
  });
}).listen(8888);
//var client  = mqtt.connect('mqtt://3egreenserver.ddns.net:1999', {clientId: "mqtt2345"})
var client = mqtt.connect('mqtt://mqtt.aengin.com:1883', {clientId: "mqtt_wschang"})

client.on('connect', function () {
  console.log("connect success");
  ['Subscribe_2471895BF9B4', 'Subscribe_2471895BF6EA', 'Subscribe_2471895BF694',
    'Subscribe_2471895BF6E6', 'Subscribe_2471895BF9A4', 'Subscribe_2471895BF6EE',
    'Subscribe_2471895BF6AE', 'Subscribe_2471895BF9BC', 'Subscribe_2471895BF6AA',
    'Subscribe_2471895BF796', 'Subscribe_2471895BF9AC'].forEach((v) => {
    console.log('subscribe ' + v);
    client.subscribe(v);
  });
})

client.on('message', function (topic, message) {
  // message is Buffer
  console.log('receive message from ' + topic);
  var time = [process.hrtime()];
  message = JSON.stringify(JSON.parse(message.toString()).map((device) => {
    device.Address = device.Address.split(':').join('');
    return {Address: device.Address, RSSI: device.RSSI};
  }));
  time.push(process.hrtime(time[0]));
  ws.connections.forEach((conn) => {
    var uuid = new UUID(3, "ns:DNS", topic);
//    console.log(uuid.format());
    conn.sendText(uuid.format() + "\n" + message.toString() + "\n" + topic);
  });
  time.push(process.hrtime(time[0]));
  fs.appendFileSync(logname, [time[1], time[2] - time[1], time[2]].join(',')+"\n");
//  client.end()
})