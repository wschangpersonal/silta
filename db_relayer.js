var dateFormat = require('dateformat');
const path = require('path');
var express = require('express');
var app = express();
var fs = require('fs');
var logname = './delay-log.csv';
fs.appendFileSync(logname, '', {flag: 'a+'});
var log_B2R = -1;
var log_DBSetup = -1;
var MongoClient = require('mongodb').MongoClient;
log_DBSetup = (new Date()).getTime();
class Relayer {
  constructor(collection) {
    this.collection = collection;
    this.collectData = function (last_update_time, callback) {
//      console.log("Collect "+new Date(last_update_time).toISOString());
      this.collection.aggregate([
        {$unwind: "$conn_beacon"},
        {$match: {'conn_beacon.time': {$gt: new Date(last_update_time)}}},
        {$group: {
            _id: '$_id',
            agentId: {$first: "$agentId"},
            max_time: {$max: "$conn_beacon.time"},
            conn_beacon: {
              $push: {
                time: "$conn_beacon.time",
                rssi: "$conn_beacon.rssi",
                beacon: "$conn_beacon.beacon",
                uuid: "$conn_beacon.uuid"
              }
            }
          }
        }
      ]).toArray(function (err, docs) {
        var data = JSON.stringify(docs.map(function (hub) {
          return [hub.agentId, hub.max_time, hub.conn_beacon.map(function (beacon) {
              var hex = beacon.uuid.buffer.toString('hex');
              return [
                beacon.rssi,
                beacon.beacon,
                [hex.substr(0, 8), hex.substr(8, 4), hex.substr(12, 4), hex.substr(16, 4), hex.substr(20)].join('-'),
                beacon.time ? dateFormat(beacon.time.toInt ? beacon.time.getHighBits() : beacon.time, "yyyy-mm-dd HH:MM:ss") : '----/--/-- --:--:--'
              ];
            })];
        }));
        callback.call(this, data);
      });
    };
    this.ws = require('nodejs-websocket').createServer((conn) => {
      console.log('New Websocket connected.');
      conn.on('text', (str) => {
        let [client_time, last_update_time] = str.split(" ");
        log_B2R = (new Date()).getTime() - (+client_time);
//        console.log("fetch " + dateFormat(last_update_time, "yyyy-mm-dd HH:MM:ss"));
        this.collectData(last_update_time, function (result) {
          conn.send(result);
        });
      });
      conn.on('close', function () {
        console.log('Websocket closed.');
      });
    }).listen(8888);
  }
}
MongoClient.connect('mongodb://ble.aengin.com:27017/courier', function (err, db) {
  log_DBSetup = (new Date()).getTime() - log_DBSetup;
  console.log('MongoDB connection established.');
  var collection = db.collection('hub_info');
  new Relayer(collection);
  app.use(express.static('public'));
  app.set('views', __dirname+'\\node_views');
  app.engine('ejs', require('ejs').renderFile);
  app.set('view engine', 'ejs');

  app.get('/', function (req, res) {
    collection.find({}).toArray(function (err, docs) {
      var data = docs.map(function (hub) {
        return [hub.agentId, hub.max_time, hub.conn_beacon.map(function (beacon) {
            var hex = beacon.uuid.buffer.toString('hex');
            return [
              beacon.rssi,
              beacon.beacon,
              [hex.substr(0, 8), hex.substr(8, 4), hex.substr(12, 4), hex.substr(16, 4), hex.substr(20)].join('-'),
              beacon.time ? dateFormat(beacon.time.toInt ? beacon.time.getHighBits() : beacon.time, "yyyy-mm-dd HH:MM:ss") : '----/--/-- --:--:--'
            ];
          })];
      });
      res.render('index', {hubs: data});
    });
  });
  app.listen(8880);
  console.log('Port 8880 is accupied for current usage!');
});